/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: 
 *
 * Created on 31 de julio de 2020, 10:11
 */

#include <iostream>
#include <cstdlib>

using namespace std;

void pausa();

int main() {
    bool bandera = true;
    int tecla;
    int n = 0;
    float codigbus;
    float codigo[n];
    float aporte1[n];
    float aporte2[n];
    float sum = 0;
    do {

        cin.clear();
        cout << "SISTEMA DE CALIFICACIONES" << endl;
        cout << "==== ==== ====== =======" << endl << endl;
        cout << "\t1 .- INGRESAR NUEVA CALIFICACION" << endl;
        cout << "\t2 .- MODIFICAR CALIFICCIÓN" << endl;
        cout << "\t3 .- ELIMINAR CALIFICACIÓN" << endl;
        cout << "\t4 .- SALIR" << endl << endl;
        cout << "==== ==== ====== =======" << endl << endl;
        cout << "Elije una opcion: ";

        cin >> tecla;

        switch (tecla) {
            case 1:

                cout << "INGRESAR NUEVA CALIFICACIÓN\n";
                cout << "==== ==== ====== =======" << endl;
                cout << "Codigo de estudiante: ";
                cin >> codigo[n];
                cout << "Aporte 1: ";
                cin >> aporte1[n];
                cout << "Aporte 2: ";
                cin >> aporte2[n];

                cout << "Calificacion ingresada correctamente\n" << endl;
                sum = 0;
                cout << "CALIFICACIONES INGRESADAS\n";
                cout << "==== ==== ====== =======" << endl;
                cout << "Codigo Aporte 1  Aporte 2  Total/30\n";
                cout << "---------------------------------------\n";

                for (int i = 0; i <= n; i++) {
                    cout << codigo[i] << "       " << aporte1[i] << "       " << aporte2[i] << "        " << aporte1[i] + aporte2[i] << "\n";
                    sum = sum + (aporte1[i] + aporte2[i]);
                }
                if (n == 0) {
                    sum = sum;
                } else {
                    sum = sum / (n + 1);
                }
                cout << "\nPromedio " << sum << "/30" << "\n" << endl;
                n++;
                pausa();
                break;
            case 2:
                cout << "MODIFICAR APORTES DEL ESTUDIANTE\n";
                cout << "==== ==== ====== =======" << endl;
                cout << "Ingrese el Codigo del estudiante para modificar aportes: ";
                cin >> codigbus;
                for (int i = 0; i < n; i++) {
                    if (codigbus == codigo[i]) {
                        cout << "Aporte 1: ";
                        cin >> aporte1[i];
                        cout << "Aporte 2: ";
                        cin >> aporte2[i];
                        bandera = true;
                        break;
                    } else {
                        bandera = false;
                    }
                }
                if (bandera == false) {
                    cout << "Calificación no encontrada"<< endl;
                } else {
                    sum = 0;
                    cout << "CALIFICACIONES INGRESADAS\n";
                    cout << "==== ==== ====== =======" << endl;
                    cout << "Codigo Aporte 1  Aporte 2  Total/30\n";
                    cout << "---------------------------------------\n";
                    for (int i = 0; i < n; i++) {
                        cout << codigo[i] << "       " << aporte1[i] << "       " << aporte2[i] << "        " << aporte1[i] + aporte2[i] << "\n";
                        sum = sum + (aporte1[i] + aporte2[i]);
                    }
                    if (n == 0) {
                        sum = sum;
                    } else {
                        sum = sum / n;
                    }
                    cout << "\nPromedio " << sum << "/30" << "\n" << endl;
                }
                pausa();
                break;

            case 3:
                cout << "ELIMINAR ESTUDIANTE\n";
                cout << "==== ==== ====== =======" << endl;
                cout << "Ingrese el Codigo del estudiante para eliminar: ";
                cin >> codigbus;
                for (int i = 0; i < n; i++) {
                    if (codigbus == codigo[i]) {
                        for (int j = i; j < n - 1; j++) {
                            codigo[j] = codigo[j + 1];
                            aporte1[j] = aporte1[j + 1];
                            aporte2[j] = aporte2[j + 1];
                        }
                        bandera=true;
                        n--;
                        break;
                    } else {
                        bandera = false;
                    }
                }
                if (bandera) {
                    sum = 0;
                    cout << "CALIFICACIONES INGRESADAS\n";
                    cout << "==== ==== ====== =======" << endl;
                    cout << "Codigo Aporte 1  Aporte 2  Total/30\n";
                    cout << "---------------------------------------\n";
                    for (int i = 0; i < n; i++) {
                        cout << codigo[i] << "       " << aporte1[i] << "       " << aporte2[i] << "        " << aporte1[i] + aporte2[i] << "\n";
                        sum = sum + (aporte1[i] + aporte2[i]);
                    }
                    if (n == 0) {
                        sum = sum;
                    } else {
                        sum = sum / n;
                    }
                    cout << "\nPromedio " << sum << "/30" << "\n" << endl;
                } else {
                    cout << "calificacion no encontrada"<< endl;
                }
                pausa();
                break;

            case 4:
                break;

            default:
                cout << "Opcion no valida.\a\n" << endl;
                pausa();
                break;
        }
    } while (tecla != 4);

    return 0;
}

void pausa() {
    //    cout << "Pulsa una tecla para continuar...";
    getwchar();
    getwchar();
}

